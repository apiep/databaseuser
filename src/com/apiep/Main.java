package com.apiep;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.apiep.model.User;
import com.apiep.util.Database;

public class Main {
	private Database db;
	private BufferedReader reader;

	public Main() {
		db = new Database();
		reader = new BufferedReader(new InputStreamReader(System.in));
	}

	/**
	 * Menampilkan semua pengguna yang berada
	 * di database
	 */
	public void printAllUser() {
		List<User> user = db.getUser();
		if(user.size() == 0){
			System.out.println("Tidak ada pengguna di database");
			return;
		}
		System.out.println("+-----------+----------------------------------------------+");
		for (int i=0; i<user.size(); i++) {
			System.out.println("| ID        | " + user.get(i).getId());
			System.out.println("| Username  | " + user.get(i).getUserName());
			System.out.println("| Firstname | " + user.get(i).getFirstName());
			System.out.println("| Lastname  | " + user.get(i).getLastName());
			System.out.println("| Email     | " + user.get(i).getEmail());
			System.out.println("+-----------+----------------------------------------------+");
		}
	}

	public void tambahPengguna() {
		User user = new User();
		try {
			System.out.println("===================================");
			System.out.print("Username : ");
			user.setUserName(reader.readLine());
			System.out.print("Firstname : ");
			user.setFirstName(reader.readLine());
			System.out.print("Lastname : ");
			user.setLastName(reader.readLine());
			System.out.print("Email : ");
			user.setEmail(reader.readLine());
			db.addUser(user);
			System.out.println("Pengguna " + user.getFirstName()
					+ " berhasil ditambahkan");
			System.out.println("===================================");
		} catch (IOException ex) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public void printUser() {
		System.out.println("===================================");
		System.out.print("Masukkan ID pengguna : ");
		Long id = 0L;
		try {
			id = Long.valueOf(reader.readLine());
		} catch (IOException ex) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
		}
		User user = db.getUserById(id);
		if (user == null) {
			System.out.println("Pengguna dengan id " + id
					+ " tidak ada di database");
			System.out.println("===================================");
			return;
		} else {
			System.out.println("ID\t\t=> " + user.getId());
			System.out.println("Username\t=> " + user.getUserName());
			System.out.println("Firstname\t=> " + user.getFirstName());
			System.out.println("Lastname\t=> " + user.getLastName());
			System.out.println("Email\t\t=> " + user.getEmail());
			System.out.println("===================================");
			return;
		}
	}

	public void updateUser() {

		Long id = 0L;
		String username = "";
		String firstname = "";
		String lastname = "";
		String email = "";
		try {
			System.out.println("===================================");
			System.out.print("Masukkan ID pengguna yg ingin diperbarui : ");
			id = Long.valueOf(reader.readLine());
			User user = db.getUserById(id);
			if (user == null) {
				System.out.println("Pengguna dengan ID " + id
						+ " tidak ada di database");
				System.out.println("===================================");
				return;
			} else {
				System.out.print("Masukkan username baru ["
						+ user.getUserName() + "] :");
				username = reader.readLine();
				user.setUserName(username.isEmpty() ? user.getUserName()
						: username);

				System.out.print("Masukkan firstname baru ["
						+ user.getFirstName() + "] :");
				firstname = reader.readLine();
				user.setFirstName(firstname.isEmpty() ? user.getFirstName()
						: firstname);

				System.out.print("Masukkan lastname baru ["
						+ user.getLastName() + "] :");
				lastname = reader.readLine();
				user.setLastName(lastname.isEmpty() ? user.getLastName()
						: lastname);

				System.out.print("Masukkan email baru [" + user.getEmail()
						+ "] :");
				email = reader.readLine();
				user.setEmail(email.isEmpty() ? user.getEmail() : email);

				db.updateUser(user);
				System.out.println("Pengguna " + user.getUserName()
						+ " berhasil diperbarui");
				System.out.println("===================================");
			}
		} catch (IOException ex) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
		}

	}

	public void deleteUser() {
		Long id = 0L;
		System.out.println("===================================");
		System.out.print("Masukkan ID pengguna yg mau dihapus : ");
		try {
			id = Long.valueOf(reader.readLine());
		} catch (IOException ex) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
		}
		User user = db.getUserById(id);
		if (user == null) {
			System.out.println("Pengguna dengan ID " + id
					+ " tidak ada di database");
			return;
		} else {
			db.deleteUser(user);
			System.out.println("Pengguna " + user.getUserName()
					+ " berhasil dihapus");
			System.out.println("===================================");
			return;
		}
	}

	public void mainMenu() {
		int inputUser = 0;
		do {
			System.out.println("1. Tambah pengguna");
			System.out.println("2. Lihat semua pengguna");
			System.out.println("3. Lihat pengguna berdasarkan ID");
			System.out.println("4. Perbarui data pengguna");
			System.out.println("5. Hapus pengguna");
			System.out.println("0. Keluar");
			System.out.print("=> ");
			try {
				inputUser = Integer.valueOf(reader.readLine());
			} catch (IOException ex) {
				Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null,
						ex);
			}
			switch (inputUser) {
			case 1:
				tambahPengguna();
				break;
			case 2:
				printAllUser();
				break;
			case 3:
				printUser();
				break;
			case 4:
				updateUser();
				break;
			case 5:
				deleteUser();
				break;
			case 0:
				System.out.println("Terima kasih");
				break;
			default:
				System.out.println("Masukan anda salah");
				break;
			}
		} while (inputUser != 0);
	}

	public static void main(String[] args) {
		Main main = new Main();
		main.mainMenu();
	}
}
