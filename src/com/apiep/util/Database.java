package com.apiep.util;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteJDBCLoader;

import com.apiep.model.User;

public class Database {
	private Connection conn;
	private Statement st;
	private PreparedStatement pst;

	private String url = "jdbc:sqlite:/";

	public Database() {
		File programHome;
		try {
			programHome = new File(System.getProperty("user.home")
					+ "/.DatabaseUser");
			if (!programHome.exists()) {
				programHome.mkdir();
			}
			SQLiteJDBCLoader.initialize();
			conn = new SQLiteConfig().createConnection(url
					+ programHome.getAbsolutePath() + "/database.db");
			initDb();
		} catch (SQLException ex) {
			Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null,
					ex);
		}
	}

	private void initDb() {
		String sql = "CREATE TABLE IF NOT EXISTS user ("
				+ " id integer primary key autoincrement,"
				+ " username text not null," + " firstname text,"
				+ " lastname text," + " email text" + ")";
		try {
			st = conn.createStatement();
			st.executeUpdate(sql);
		} catch (SQLException ex) {
			Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null,
					ex);
		}
	}

	public void addUser(User user) {
		String sql = "INSERT INTO user VALUES (null,?,?,?,?)";
		try {
			pst = conn.prepareStatement(sql, new String[] { "id" });
			pst.setString(1, user.getUserName());
			pst.setString(2, user.getFirstName());
			pst.setString(3, user.getLastName());
			pst.setString(4, user.getEmail());
			pst.executeUpdate();
			ResultSet rs = pst.getGeneratedKeys();
			if (rs.next()) {
				user.setId(rs.getLong(1));
			}
		} catch (SQLException ex) {
			Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null,
					ex);
		}
	}

	public List<User> getUser() {
		List<User> list = new ArrayList<User>();
		try {
			String sql = "SELECT * FROM user";
			st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				User user = new User();
				user.setId(rs.getLong("id"));
				user.setUserName(rs.getString("username"));
				user.setFirstName(rs.getString("firstname"));
				user.setLastName(rs.getString("lastname"));
				user.setEmail(rs.getString("email"));
				list.add(user);
			}
		} catch (SQLException ex) {
			Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null,
					ex);
		}
		return list;
	}

	public void updateUser(User user) {
		try {
			String sql = "UPDATE user SET username=?,firstname=?,lastname=?,email=? WHERE id=?";
			pst = conn.prepareStatement(sql);
			pst.setString(1, user.getUserName());
			pst.setString(2, user.getFirstName());
			pst.setString(3, user.getLastName());
			pst.setString(4, user.getEmail());
			pst.setLong(5, user.getId());
			pst.executeUpdate();
		} catch (SQLException ex) {
			Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null,
					ex);
		}
	}

	public void deleteUser(User user) {
		try {
			String sql = "DELETE FROM user WHERE id=?";
			pst = conn.prepareStatement(sql);
			pst.setLong(1, user.getId());
			pst.executeUpdate();
		} catch (SQLException ex) {
			Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null,
					ex);
		}
	}

	public User getUserById(Long id) {
		User user = null;
		try {
			String sql = "SELECT * FROM user WHERE id=?";
			pst = conn.prepareStatement(sql);
			pst.setLong(1, id);
			ResultSet rs = pst.executeQuery();
			if (rs.next()) {
				user = new User();
				user.setId(rs.getLong("id"));
				user.setUserName(rs.getString("username"));
				user.setFirstName(rs.getString("firstname"));
				user.setLastName(rs.getString("lastname"));
				user.setEmail(rs.getString("email"));
			}
		} catch (SQLException ex) {
			Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null,
					ex);
		}
		return user;
	}
}
